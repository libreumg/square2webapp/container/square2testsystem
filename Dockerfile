FROM python:3.9.15
WORKDIR /tests
RUN apt update -y && apt install firefox-esr -y
ADD https://gitlab.com/libreumg/square2webapp/Square2/-/commits/robot/ /tmp/
RUN git clone --branch robot-ci https://gitlab.com/libreumg/square2webapp/Square2.git \
 && mv Square2/src/test/acceptancetest/* . \
 && rm -r Square2 \
 && pip install --no-cache-dir -r requirements.txt
RUN webdrivermanager gecko:v0.31.0
ENV ROBOT_USER=administrator
ENV ROBOT_PASSWORD=password
ENV ROBOT_SERVER=tomcat:8080
ENV ROBOT_LOGLEVEL=debug
ENTRYPOINT ["python3", "run.py"]
